<section id="creatives" class="page-anchor">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <h1 class="text-center team-headline centered-headline">Meet the Creatives</h1>
            </div>
        </div>
        <div class="row">
            <?php if (have_rows('team_repeater')): ?>
                <?php while (have_rows('team_repeater')): the_row(); ?>
                    <div class="col-sm-3 col-xs-6 text-center team-wrapper">
                        <img class="img-responsive team-image" src="<?php echo get_sub_field('image'); ?>"/>

                        <h4 class="no-bottom team-name"><strong><?php echo get_sub_field('name'); ?></strong></h4>

                        <p class="team-title"><?php echo get_sub_field('title'); ?></p>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</section>