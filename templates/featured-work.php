<section id="portfolio" class="page-anchor">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2 text-center">
                <h1 class="centered-headline"><?php echo get_field('testimonial_title'); ?></h1>
                <p class="lead"><?php echo get_field('testimonial_sub_description'); ?></p>
            </div>
        </div>
        <div class="row">
            <?php query_posts('cat=0'); ?>
            <?php while (have_posts()) :
                the_post(); ?>
                <div class="col-sm-4">
                    <a class="featured-work-anchor" href="<?php echo get_the_permalink(); ?>">
                        <div class="panel panel-default featured-work featured-work-portfolio" style=" background-image: url('<?php echo get_field('detailed_work_image_one');?>') ;">
                            <div class="panel-body">
                                <p class="text-center no-bottom featured-work-title"><strong><?php echo the_title(); ?></strong></p>
                            </div>
                        </div>
                    </a>
                </div>
            <?php endwhile; ?>
            <?php wp_reset_query(); ?>
        </div>
    </div>
</section>

