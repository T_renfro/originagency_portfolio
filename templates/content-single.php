<?php while (have_posts()) : the_post(); ?>
    <article <?php post_class(); ?>>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                                <img class="detailed-work-image img-responsive"
                                     src="<?php echo get_field('detailed_work_image_one'); ?>">
                        <?php if (get_field('detailed_work_image_two')): ?>
                                    <img class="detailed-work-image img-responsive"
                                         src="<?php echo get_field('detailed_work_image_two'); ?>">
                        <?php endif; ?>
                        <?php if (get_field('detailed_work_image_three')): ?>
                                    <img class="detailed-work-image img-responsive"
                                         src="<?php echo get_field('detailed_work_image_three'); ?>">
                        <?php endif; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8">
                        <h1 class="underline-headline"><strong><?php the_title(); ?></strong></h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <ul class="list-unstyled">
                            <?php if (have_rows('detailed_work_list_repeater')): ?>
                                <?php while (have_rows('detailed_work_list_repeater')): the_row(); ?>
                                    <li>
                                        <strong><?php echo get_sub_field('list_bold'); ?></strong> <?php echo get_sub_field('list_regular'); ?>
                                    </li>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </ul>
                    </div>
                    <div class="col-sm-5">
                        <?php echo the_content(); ?>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="underline-headline text-center">See more of our work</h2>
                        <?php
                        query_posts('posts_per_page=3');
                        ?>
                        <?php while (have_posts()) :
                            the_post(); ?>
                            <div class="col-sm-4">
                                <a class="featured-work-anchor" href="<?php echo get_the_permalink(); ?>">
                                    <div class="panel panel-default featured-work featured-work-portfolio"
                                         style=" background-image: url('<?php echo get_field('detailed_work_image_one'); ?>') ;">
                                        <div class="panel-body">
                                            <p class="text-center no-bottom featured-work-title">
                                                <strong><?php echo the_title(); ?></strong></p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php endwhile; ?>
                        <?php wp_reset_query(); ?>
                    </div>
                </div>
            </div>
        </section>
    </article>
<?php endwhile; ?>
