<div class="jumbotron hero-section contact page-anchor" id="contact">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><?php echo get_field('contact_title'); ?></h1>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <p><?php echo get_field('contact_description'); ?></p>
                <div class="contact-list">
                    <div class="row">
                        <a href="tel:314-652-1234">
                            <div class="col-xs-2 col-sm-1">
                        <span class="glyphicon glyphicon-earphone" aria-hidden="true">
                            </div>
                            <div class="col-xs-10 col-sm-10">
                                <p>314.652.1234</p>
                            </div>
                        </a>
                    </div>
                    <div class="row">
                        <a target="blank"
                           href="https://www.google.com/maps/place/3016+Locust+St+%23101,+St+Louis,+MO+63103/@38.6357691,-90.2224521,17z/data=!3m1!4b1!4m2!3m1!1s0x87d8b35f18ca5c71:0x6c3de08e1941e3e3">
                            <div class="col-xs-2 col-sm-1">
                                <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                            </div>
                            <div class="col-xs-10 col-sm-10">
                                <p>3016 Locust St., Suite 101. St. Louis, MO 63103</p>
                            </div>
                        </a>
                    </div>
                    <div class="row">
                        <a href="mailto:test@originagency.com">
                            <div class="col-xs-2 col-sm-1">
                                <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
                            </div>
                            <div class="col-xs-10 col-sm-10">
                                <p>Our Email</p>
                            </div>
                        </a>
                    </div>
                </div>
                <hr class="visible-xs">
            </div>
            <div class="col-sm-4 col-sm-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <img class="img-responsive maps-thumbnail"
                             src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/maps-thumbnail.png">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>