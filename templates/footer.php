<footer class="content-info" role="contentinfo">
  <div class="container">
    <div class="row">
      <div class="col-sm-8">
        <small><p>This work is licensed under a Creative Commons License. <br>&copy; Copyright <?php echo date("Y"); ?> Origin Agency. All Rights Reserved.</p></small>
      </div>
      <div class="col-sm-4">
        <ul class="list-inline social-media text-right">
            <li><p class="text-right">Follow Us</p></li>
          <li><a href="https://twitter.com/originagency" target="blank"><i class="fa fa-twitter"></i></a></li>
          <li><a href="https://www.facebook.com/originagency" target="blank"><i class="fa fa-facebook"></i></a></li>
          <li><a href="https://www.linkedin.com/company/origin-agency" target="blank"><i class="fa fa-linkedin"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</footer>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/dist/scripts/jquery.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/dist/scripts/jquery.flexslider.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/dist/scripts/interactive.js"></script>
