<div class="jumbotron main-hero hero-section page-anchor" id="about">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2 text-center">
                <h1><?php echo get_field('headline'); ?></h1>
                <p><?php echo get_field('description'); ?></p>
            </div>
        </div>
    </div>
</div>