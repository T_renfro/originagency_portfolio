<div id="testimonials" class="jumbotron hero-section testimonial page-anchor">
    <section class="testimonial-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="flexslider">
                        <ul class="slides">
                            <?php if (have_rows('testimonial_repeater')): ?>
                                <?php while (have_rows('testimonial_repeater')): the_row(); ?>
                                    <li>
                                        <p class="lead">&ldquo;<?php echo get_sub_field('quote'); ?>&rdquo;</p>

                                        <h3>&mdash;<?php echo get_sub_field('name'); ?></h3>

                                        <p class="lead"><?php echo get_sub_field('title'); ?></p>
                                    </li>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
